<?php
include ('koneksi.php');

//getting id from url
$id = $_GET['id'];

//selecting data associated with this particular id
$result = $conn->collection->findOne(array('_id' => new MongoDB\BSON\ObjectID($id)));
$namaproduk = $result['namaproduk'];
$harga = $result['harga'];
$stok = $result['stok'];
$status = $result['status'];
?>

<!DOCTYPE html>
<html lang="en">
<head>
 <meta charset="UTF-8">
 <meta http-equiv="X-UA-Compatible" content="IE=edge">
 <meta name="viewport" content="width=device-width, initial-scale=1.0">
 <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

 <title>Document</title>
</head>
<body>
<?php
  include ('template/navbar.php');
  ?>
<div class="container my-1">


<form method="post" action="<?php $conn->updateData()?>" >
<input type="hidden" name="id" value="<?= $id; ?>">
  <div class="row mb-3">
    <label for="namaproduk" class="col-sm-2 col-form-label">Nama Produk</label>
    <div class="col-sm-6">
      <input type="text" name="namaproduk" class="form-control" id="namaproduk" value="<?= $namaproduk; ?>">
    </div>
  </div>
  <div class="row mb-3">
    <label for="namaproduk" class="col-sm-2 col-form-label">Harga</label>
    <div class="col-sm-6">
      <input type="text" name="harga" class="form-control" id="namaproduk" value="<?= $harga; ?>">
    </div>
  </div>
  <div class="row mb-3">
    <label for="namaproduk" class="col-sm-2 col-form-label">Stok</label>
    <div class="col-sm-6">
      <input type="text" name="stok" class="form-control" id="namaproduk" value="<?= $stok; ?>">
    </div>
  </div>
  <div class="row mb-3">
    <label for="namaproduk" class="col-sm-2 col-form-label">Status</label>
    <div class="col-sm-6">
      <input type="text" name="status" class="form-control" id="namaproduk" value="<?= $status; ?>">
    </div>
  </div>
  <button type="submit" name="submit" class="btn btn-primary" onClick="alert('Data Berhasil Diubah')">Ubah Data</button>
</form>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

</div>
</body>
</html>