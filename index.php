<?php
include ('koneksi.php');
$produk = $conn->ambilData();

?>

<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    <title>Hello, world!</title>
  </head>
  <body>
  <?php
  include ('template/navbar.php');
  ?>
<div class="container my-1">
 <h1 class="display-4">Data Produk</h1>
 <table class="table">
  <thead class="table-dark">
    <tr>
     <th scope="col">No</th>
     <th scope="col">Nama Produk</th>
     <th scope="col">Harga</th>
     <th scope="col">Stok</th>
     <th scope="col">Status</th>
     <th scope="col">Maintanance</th>
    </tr>
   </thead>
   <tbody>
<?php
$nomor = 1;
foreach($produk as $row):

?>

    <tr>
     <th scope="row"><?= $nomor++; ?></th>
     <td><?= $row['namaproduk']; ?></td>
     <td><?= $row['harga']; ?></td>
     <td><?= $row['stok']; ?></td>
     <td><?= $row['status']; ?></td>
     <td>
      <a href="editData.php?id=<?= $row['_id']; ?>" class="btn btn-warning btn-sm">Edit</a>
      <form action="<?php $conn->deleteData()?>" method="post" class="d-inline deletedata ">
       <input type="hidden" name="id" value="<?= $row['_id']; ?>">
       <button type="submit" name="sub" class="btn btn-danger btn-sm " >Hapus</button>
      </form>
     </td>
    </tr>
   
    <?php
    endforeach;
    ?>
   </tbody>
  </table>


  <a href="tambahdata.php" class="btn btn-dark btn-sm">Tambah Data</a>

 </div>
  <script>

   // $(document).ready(function(){

   //  $('.deletedata').submit(function(e){
   //   e.preventDefault();
     
   //   $.ajax({
   //    type: $(this).attr('method'),
   //    url: $(this).attr('action'),
   //    data: $(this).serialize(),
   //    success: function() {

   //    }
   //   });
   //  })
   // });
   // function readData(){
   //  $('.table').load(this);
   // }
  </script>
  <!-- Optional JavaScript; choose one of the two! -->
  <!-- Option 1: Bootstrap Bundle with Popper -->
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
    -->
  </body>
</html>
