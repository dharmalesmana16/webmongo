<?php
require 'vendor/autoload.php';

class koneksi{
 public $client;
 public $db;
 public $collection;
 public function __construct(){

  $this->client = new MongoDB\Client;
  $this->db = $this->client->nuansaintipersada;
  $this->collection = $this->db->produk;
 }

 public function ambilData(){
   return $this->collection->find();
 }
 public function updateData(){
  if(isset($_POST['submit'])){
   $id=$_POST['id'];
   $dataTambah = [
    'namaproduk' => $_POST['namaproduk'],
    'stok' => $_POST['stok'],
    'harga' => $_POST['harga'],
    'status' => $_POST['status']
   ];
   $result = $this->collection->updateOne(
    array('_id' => new MongoDB\BSON\ObjectID($id)),
    array('$set' => $dataTambah)
   );
   if($result == True){
    header('Location:index.php');
   }
  }
 }
 public function tambahdata(){

 if(isset($_POST['submit'])){
  $nama=$_POST['namaproduk'];
 $harga=$_POST['harga'];
 $stok=$_POST['stok'];
 $status=$_POST['status'];
 

 $hasilTambah =  $this->collection->insertOne([
  'namaproduk' => $nama,
  'harga' => $harga,
  'stok' => $stok,
  'status' => $status

 ]);
 if($hasilTambah->getInsertedCount()>0){
  echo "<script>alert(Data berhasil Ditambah)</script>";
  header("Location: index.php"); }
  else{
  echo "Data gagal ditambahkan ";
 }
 }
 

 
 }

 public function deleteData(){
 if(isset($_POST['sub'])){

  $this->collection->deleteOne(['_id' => new MongoDB\BSON\ObjectID($_POST['id'])]);
  header('Location:index.php');
 }
 }
 // public function update($id){
  
 // }
}
$conn = new koneksi();

?>